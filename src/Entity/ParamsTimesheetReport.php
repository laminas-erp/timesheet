<?php

namespace Lerp\Timesheet\Entity;

use Bitkorn\Trinket\Entity\ParamsBase;

class ParamsTimesheetReport extends ParamsBase
{
    protected array $orderFieldsAvailable = ['timesheet_time_start', 'timesheet_time_end'];
}
