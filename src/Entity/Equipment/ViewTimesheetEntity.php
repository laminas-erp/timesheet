<?php

namespace Lerp\Timesheet\Entity\Equipment;

use Bitkorn\Trinket\Entity\AbstractEntity;

class ViewTimesheetEntity extends AbstractEntity
{
    public array $mapping = [
        'timesheet_uuid'           => 'timesheet_uuid',
        'equipment_uuid'           => 'equipment_uuid',
        'timesheet_time_start'     => 'timesheet_time_start',
        'timesheet_time_start_f'   => 'timesheet_time_start_f',
        'timesheet_time_end'       => 'timesheet_time_end',
        'timesheet_time_end_f'     => 'timesheet_time_end_f',
        'timesheet_div_minute'     => 'timesheet_div_minute',
        'user_uuid_create_start'   => 'user_uuid_create_start',
        'user_uuid_create_end'     => 'user_uuid_create_end',
        'equipment_type'           => 'equipment_type',
        'equipment_locked'         => 'equipment_locked',
        'location_place_uuid'      => 'location_place_uuid',
        'equipment_price_per_hour' => 'equipment_price_per_hour',
        'location_place_id'        => 'location_place_id',
        'location_place_name'      => 'location_place_name',
        'location_place_label'     => 'location_place_label',
        'user_uuid'                => 'user_uuid',
        'user_login'               => 'user_login',
        'user_active'              => 'user_active',
        'user_email'               => 'user_email',
        'user_lang_iso'            => 'user_lang_iso',
        'user_details_name_first'  => 'user_details_name_first',
        'user_details_name_last'   => 'user_details_name_last',
        'user_details_tel_land'    => 'user_details_tel_land',
        'user_details_tel_mobile'  => 'user_details_tel_mobile',
        'user_details_tel_fax'     => 'user_details_tel_fax',
        'user_details_brand_pre'   => 'user_details_brand_pre',
        'user_details_brand_post'  => 'user_details_brand_post',
        'user_details_job'         => 'user_details_job',
        'user_login_create_start'  => 'user_login_create_start',
        'user_login_create_end'    => 'user_login_create_end',
    ];

    public function getTimesheetUuid(): string
    {
        if (!isset($this->storage['timesheet_uuid'])) {
            return '';
        }
        return $this->storage['timesheet_uuid'];
    }

    public function setTimesheetUuid(string $timesheetUuid): void
    {
        $this->storage['timesheet_uuid'] = $timesheetUuid;
    }

    public function getEquipmentUuid(): string
    {
        if (!isset($this->storage['equipment_uuid'])) {
            return '';
        }
        return $this->storage['equipment_uuid'];
    }

    public function setEquipmentUuid(string $equipmentUuid): void
    {
        $this->storage['equipment_uuid'] = $equipmentUuid;
    }

    public function getTimesheetTimeStart(): string
    {
        if (!isset($this->storage['timesheet_time_start'])) {
            return '';
        }
        return $this->storage['timesheet_time_start'];
    }

    public function setTimesheetTimeStart(string $timesheetTimeStart): void
    {
        $this->storage['timesheet_time_start'] = $timesheetTimeStart;
    }

    public function getTimesheetTimeStartF(): string
    {
        if (!isset($this->storage['timesheet_time_start_f'])) {
            return '';
        }
        return $this->storage['timesheet_time_start_f'];
    }

    public function setTimesheetTimeStartF(string $timesheetTimeStartF): void
    {
        $this->storage['timesheet_time_start_f'] = $timesheetTimeStartF;
    }

    public function getTimesheetTimeEnd(): string
    {
        if (!isset($this->storage['timesheet_time_end'])) {
            return '';
        }
        return $this->storage['timesheet_time_end'];
    }

    public function setTimesheetTimeEnd(string $timesheetTimeEnd): void
    {
        $this->storage['timesheet_time_end'] = $timesheetTimeEnd;
    }

    public function getTimesheetTimeEndF(): string
    {
        if (!isset($this->storage['timesheet_time_end_f'])) {
            return '';
        }
        return $this->storage['timesheet_time_end_f'];
    }

    public function setTimesheetTimeEndF(string $timesheetTimeEndF): void
    {
        $this->storage['timesheet_time_end_f'] = $timesheetTimeEndF;
    }

    public function getTimesheetDivMinute(): int
    {
        if (!isset($this->storage['timesheet_div_minute'])) {
            return 0;
        }
        return $this->storage['timesheet_div_minute'];
    }

    public function setTimesheetDivMinute(int $timesheetDivMinute): void
    {
        $this->storage['timesheet_div_minute'] = $timesheetDivMinute;
    }

    public function getUserUuidCreateStart(): string
    {
        if (!isset($this->storage['user_uuid_create_start'])) {
            return '';
        }
        return $this->storage['user_uuid_create_start'];
    }

    public function setUserUuidCreateStart(string $userUuidCreateStart): void
    {
        $this->storage['user_uuid_create_start'] = $userUuidCreateStart;
    }

    public function getUserUuidCreateEnd(): string
    {
        if (!isset($this->storage['user_uuid_create_end'])) {
            return '';
        }
        return $this->storage['user_uuid_create_end'];
    }

    public function setUserUuidCreateEnd(string $userUuidCreateEnd): void
    {
        $this->storage['user_uuid_create_end'] = $userUuidCreateEnd;
    }

    public function getEquipmentType(): string
    {
        if (!isset($this->storage['equipment_type'])) {
            return '';
        }
        return $this->storage['equipment_type'];
    }

    public function setEquipmentType(string $equipmentType): void
    {
        $this->storage['equipment_type'] = $equipmentType;
    }

    public function getEquipmentLocked(): bool
    {
        if (!isset($this->storage['equipment_locked'])) {
            return '';
        }
        return $this->storage['equipment_locked'];
    }

    public function setEquipmentLocked(bool $equipmentLocked): void
    {
        $this->storage['equipment_locked'] = $equipmentLocked;
    }

    public function getLocationPlaceUuid(): string
    {
        if (!isset($this->storage['location_place_uuid'])) {
            return '';
        }
        return $this->storage['location_place_uuid'];
    }

    public function setLocationPlaceUuid(string $locationPlaceUuid): void
    {
        $this->storage['location_place_uuid'] = $locationPlaceUuid;
    }

    public function getEquipmentPricePerHour(): float
    {
        if (!isset($this->storage['equipment_price_per_hour'])) {
            return '';
        }
        return $this->storage['equipment_price_per_hour'];
    }

    public function setEquipmentPricePerHour(float $equipmentPricePerHour): void
    {
        $this->storage['equipment_price_per_hour'] = $equipmentPricePerHour;
    }

    public function getLocationPlaceId(): int
    {
        if (!isset($this->storage['location_place_id'])) {
            return 0;
        }
        return $this->storage['location_place_id'];
    }

    public function setLocationPlaceId(int $locationPlaceId): void
    {
        $this->storage['location_place_id'] = $locationPlaceId;
    }

    public function getLocationPlaceName(): string
    {
        if (!isset($this->storage['location_place_name'])) {
            return '';
        }
        return $this->storage['location_place_name'];
    }

    public function setLocationPlaceName(string $locationPlaceName): void
    {
        $this->storage['location_place_name'] = $locationPlaceName;
    }

    public function getLocationPlaceLabel(): string
    {
        if (!isset($this->storage['location_place_label'])) {
            return '';
        }
        return $this->storage['location_place_label'];
    }

    public function setLocationPlaceLabel(string $locationPlaceLabel): void
    {
        $this->storage['location_place_label'] = $locationPlaceLabel;
    }

    public function getUserUuid(): string
    {
        if (!isset($this->storage['user_uuid'])) {
            return '';
        }
        return $this->storage['user_uuid'];
    }

    public function setUserUuid(string $userUuid): void
    {
        $this->storage['user_uuid'] = $userUuid;
    }

    public function getUserLogin(): string
    {
        if (!isset($this->storage['user_login'])) {
            return '';
        }
        return $this->storage['user_login'];
    }

    public function setUserLogin(string $userLogin): void
    {
        $this->storage['user_login'] = $userLogin;
    }

    public function getUserActive(): int
    {
        if (!isset($this->storage['user_active'])) {
            return 0;
        }
        return $this->storage['user_active'];
    }

    public function setUserActive(int $userActive): void
    {
        $this->storage['user_active'] = $userActive;
    }

    public function getUserEmail(): string
    {
        if (!isset($this->storage['user_email'])) {
            return '';
        }
        return $this->storage['user_email'];
    }

    public function setUserEmail(string $userEmail): void
    {
        $this->storage['user_email'] = $userEmail;
    }

    public function getUserLangIso(): string
    {
        if (!isset($this->storage['user_lang_iso'])) {
            return '';
        }
        return $this->storage['user_lang_iso'];
    }

    public function setUserLangIso(string $userLangIso): void
    {
        $this->storage['user_lang_iso'] = $userLangIso;
    }

    public function getUserDetailsNameFirst(): string
    {
        if (!isset($this->storage['user_details_name_first'])) {
            return '';
        }
        return $this->storage['user_details_name_first'];
    }

    public function setUserDetailsNameFirst(string $userDetailsNameFirst): void
    {
        $this->storage['user_details_name_first'] = $userDetailsNameFirst;
    }

    public function getUserDetailsNameLast(): string
    {
        if (!isset($this->storage['user_details_name_last'])) {
            return '';
        }
        return $this->storage['user_details_name_last'];
    }

    public function setUserDetailsNameLast(string $userDetailsNameLast): void
    {
        $this->storage['user_details_name_last'] = $userDetailsNameLast;
    }

    public function getUserDetailsTelLand(): string
    {
        if (!isset($this->storage['user_details_tel_land'])) {
            return '';
        }
        return $this->storage['user_details_tel_land'];
    }

    public function setUserDetailsTelLand(string $userDetailsTelLand): void
    {
        $this->storage['user_details_tel_land'] = $userDetailsTelLand;
    }

    public function getUserDetailsTelMobile(): string
    {
        if (!isset($this->storage['user_details_tel_mobile'])) {
            return '';
        }
        return $this->storage['user_details_tel_mobile'];
    }

    public function setUserDetailsTelMobile(string $userDetailsTelMobile): void
    {
        $this->storage['user_details_tel_mobile'] = $userDetailsTelMobile;
    }

    public function getUserDetailsTelFax(): string
    {
        if (!isset($this->storage['user_details_tel_fax'])) {
            return '';
        }
        return $this->storage['user_details_tel_fax'];
    }

    public function setUserDetailsTelFax(string $userDetailsTelFax): void
    {
        $this->storage['user_details_tel_fax'] = $userDetailsTelFax;
    }

    public function getUserDetailsBrandPre(): string
    {
        if (!isset($this->storage['user_details_brand_pre'])) {
            return '';
        }
        return $this->storage['user_details_brand_pre'];
    }

    public function setUserDetailsBrandPre(string $userDetailsBrandPre): void
    {
        $this->storage['user_details_brand_pre'] = $userDetailsBrandPre;
    }

    public function getUserDetailsBrandPost(): string
    {
        if (!isset($this->storage['user_details_brand_post'])) {
            return '';
        }
        return $this->storage['user_details_brand_post'];
    }

    public function setUserDetailsBrandPost(string $userDetailsBrandPost): void
    {
        $this->storage['user_details_brand_post'] = $userDetailsBrandPost;
    }

    public function getUserDetailsJob(): string
    {
        if (!isset($this->storage['user_details_job'])) {
            return '';
        }
        return $this->storage['user_details_job'];
    }

    public function setUserDetailsJob(string $userDetailsJob): void
    {
        $this->storage['user_details_job'] = $userDetailsJob;
    }

    public function getUserLoginCreateStart(): string
    {
        if (!isset($this->storage['user_login_create_start'])) {
            return '';
        }
        return $this->storage['user_login_create_start'];
    }

    public function setUserLoginCreateStart(string $userLoginCreateStart): void
    {
        $this->storage['user_login_create_start'] = $userLoginCreateStart;
    }

    public function getUserLoginCreateEnd(): string
    {
        if (!isset($this->storage['user_login_create_end'])) {
            return '';
        }
        return $this->storage['user_login_create_end'];
    }

    public function setUserLoginCreateEnd(string $userLoginCreateEnd): void
    {
        $this->storage['user_login_create_end'] = $userLoginCreateEnd;
    }
}
