<?php

namespace Lerp\Timesheet\Factory\Controller\Pos\Equipment;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Equipment\Service\Equipment\EquipmentService;
use Lerp\Equipment\Service\User\UserEquipService;
use Lerp\Timesheet\Controller\Pos\Equipment\TimesheetPosController;
use Lerp\Timesheet\Service\Equipment\TimesheetService;

class TimesheetPosControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new TimesheetPosController();
        $controller->setLogger($container->get('logger'));
        $config = $container->get('config');
        $controller->setApiKey($config['lerp_application']['api_key']);
        $controller->setUserEquipService($container->get(UserEquipService::class));
        $controller->setEquipmentService($container->get(EquipmentService::class));
        $controller->setTimesheetService($container->get(TimesheetService::class));
        return $controller;
    }
}
