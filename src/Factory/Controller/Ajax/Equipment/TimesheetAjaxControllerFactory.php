<?php

namespace Lerp\Timesheet\Factory\Controller\Ajax\Equipment;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Timesheet\Controller\Ajax\Equipment\TimesheetAjaxController;
use Lerp\Timesheet\Service\Equipment\TimesheetService;
use Lerp\Timesheet\Service\Equipment\TimesheetSpreadsheetServiceInterface;
use Lerp\Equipment\Service\User\UserEquipService;
use Lerp\Equipment\Service\Equipment\EquipmentService;

class TimesheetAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new TimesheetAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setTimesheetService($container->get(TimesheetService::class));
        $controller->setEquipmentService($container->get(EquipmentService::class));
        $controller->setEquipmentUserService($container->get(UserEquipService::class));
        $controller->setTimesheetSpreadsheetService($container->get(TimesheetSpreadsheetServiceInterface::class));
        return $controller;
    }
}
