<?php

namespace Lerp\Timesheet\Factory\Service\Equipment;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Timesheet\Service\Equipment\TimesheetFoWorkflowService;
use Lerp\Timesheet\Table\Equipment\TimesheetFoWorkflowTable;
use Lerp\Timesheet\Table\Equipment\ViewTimesheetFoWorkflowTable;

class TimesheetFoWorkflowServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new TimesheetFoWorkflowService();
        $service->setLogger($container->get('logger'));
        $service->setTimesheetFoWorkflowTable($container->get(TimesheetFoWorkflowTable::class));
        $service->setViewTimesheetFoWorkflowTable($container->get(ViewTimesheetFoWorkflowTable::class));
        return $service;
    }
}
