<?php

namespace Lerp\Timesheet\Factory\Service\Equipment;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Timesheet\Service\Equipment\TimesheetService;
use Lerp\Equipment\Service\User\UserEquipService;
use Lerp\Timesheet\Table\Equipment\TimesheetTable;
use Lerp\Timesheet\Table\Equipment\ViewTimesheetTable;

class TimesheetServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new TimesheetService();
        $service->setLogger($container->get('logger'));
        $service->setEquipmentUserService($container->get(UserEquipService::class));
        $service->setTimesheetTable($container->get(TimesheetTable::class));
        $service->setViewTimesheetTable($container->get(ViewTimesheetTable::class));
        return $service;
    }
}
