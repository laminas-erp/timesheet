<?php

namespace Lerp\Timesheet\Table\Equipment;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class TimesheetTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'timesheet';

    /**
     * @param string $timesheetUuid
     * @return array
     */
    public function getTimesheet(string $timesheetUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['timesheet_uuid' => $timesheetUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existTimesheetStartWithoutEnd(string $equipmentUuid): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['equipment_uuid' => $equipmentUuid])->where->isNull('timesheet_time_end');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * It looks for a timesheet start without end. If this exist then return an empty string.
     * @param string $equipmentUuid
     * @param string $userUuidCreate
     * @param string $timeStart If it is empty, the current time is used.
     * @return string The new generated timesheet_uuid or an empty string on failure.
     */
    public function insertTimesheetStart(string $equipmentUuid, string $userUuidCreate, string $timeStart = ''): string
    {
        if ($this->existTimesheetStartWithoutEnd($equipmentUuid)) {
            return '';
        }
        $insert = $this->sql->insert();
        if (empty($timeStart)) {
            $timeStart = new Expression('current_timestamp');
        }
        $uuid = $this->uuid();
        try {
            $insert->values([
                'timesheet_uuid'         => $uuid,
                'equipment_uuid'         => $equipmentUuid,
                'timesheet_time_start'   => $timeStart,
                'user_uuid_create_start' => $userUuidCreate,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $equipmentUuid
     * @param string $userUuidCreate
     * @param string $timeEnd If it is empty, the current time is used.
     * @return int Greater than 0 on success.
     */
    public function insertTimesheetEnd(string $equipmentUuid, string $userUuidCreate, string $timeEnd = ''): int
    {
        $update = $this->sql->update();
        if (empty($timeEnd)) {
            $timeEnd = new Expression('current_timestamp');
        }
        try {
            $update->set([
                'timesheet_time_end'   => $timeEnd,
                'user_uuid_create_end' => $userUuidCreate,
            ]);
            $update->where(['equipment_uuid' => $equipmentUuid])->where->isNull('timesheet_time_end');
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
