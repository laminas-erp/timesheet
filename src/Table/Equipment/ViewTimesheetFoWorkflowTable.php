<?php

namespace Lerp\Timesheet\Table\Equipment;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ViewTimesheetFoWorkflowTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_timesheet_fo_workflow';

    /**
     * @param string $factoryorderUuid
     * @return array ORDER BY the highest workflow priority THEN the first timesheet start time
     */
    public function getViewTimesheetsFoWorkflowForFactoryorder(string $factoryorderUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['factoryorder_uuid' => $factoryorderUuid]);
            $select->order('factoryorder_workflow_order_priority DESC')->order('timesheet_fo_workflow_time_start');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
