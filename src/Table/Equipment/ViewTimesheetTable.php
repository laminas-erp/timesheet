<?php

namespace Lerp\Timesheet\Table\Equipment;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Timesheet\Entity\ParamsTimesheetReport;

class ViewTimesheetTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_timesheet';

    /**
     * @param \Lerp\Timesheet\Entity\ParamsTimesheetReport $params
     * @param string $equipmentUuid
     * @param string $timeStart
     * @param string $timeEnd
     * @return array|int[] If $params->isDoCount() then int[] with one entry - the count value.
     */
    public function equipmentViewTimesheetPeriodSearch(ParamsTimesheetReport $params, string $equipmentUuid, string $timeStart, string $timeEnd): array
    {
        $select = $this->sql->select();
        try {
            if ($params->isDoCount()) {
                $select->columns(['count_times' => new Expression('COUNT(*)')]);
            }
            $select->where(['equipment_uuid' => $equipmentUuid]);
            if (!empty($timeStart)) {
                $select->where->greaterThanOrEqualTo('timesheet_time_start', $timeStart);
            }
            if (!empty($timeEnd)) {
                $select->where->lessThanOrEqualTo('timesheet_time_end', $timeEnd);
            }
            $params->computeSelect($select);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if ($params->isDoCount()) {
                    return [$result->toArray()[0]['count_times']];
                }
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        if ($params->isDoCount()) {
            return [0];
        }
        return [];
    }

    /**
     * @param \Lerp\Timesheet\Entity\ParamsTimesheetReport $params Only for setting the ORDER BY clause.
     * @param string $equipmentUuid
     * @param string $timeStart
     * @param string $timeEnd
     * @return array The resulting array from view.
     */
    public function equipmentViewTimesheetPeriod(ParamsTimesheetReport $params, string $equipmentUuid, string $timeStart, string $timeEnd): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['equipment_uuid' => $equipmentUuid]);
            if (!empty($timeStart)) {
                $select->where->greaterThanOrEqualTo('timesheet_time_start', $timeStart);
            }
            if (!empty($timeEnd)) {
                $select->where->lessThanOrEqualTo('timesheet_time_end', $timeEnd);
            }
            $params->computeSelect($select);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
