<?php

namespace Lerp\Timesheet\Table\Equipment;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Lerp\Factoryorder\Table\FactoryorderTable;
use Lerp\Factoryorder\Table\FactoryorderWorkflowTable;

class TimesheetFoWorkflowTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'timesheet_fo_workflow';

    public static array $columnsStatic = [
        'timesheet_fo_workflow_uuid',
        'equipment_uuid',
        'factory_order_workflow_uuid',
        'timesheet_fo_workflow_time_start',
        'timesheet_fo_workflow_time_end',
        'user_uuid_create_start',
        'user_uuid_create_end',
    ];

    public function startTimesheetFoWorkflow(string $equipmentUuid, string $factoryorderWorkflowUuid, string $userUuidCreate): string
    {
        $insert = $this->sql->insert();
        try {
            $uuid = $this->uuid();
            $insert->values([
                'timesheet_fo_workflow_uuid'       => $uuid,
                'equipment_uuid'                   => $equipmentUuid,
                'factoryorder_workflow_uuid'      => $factoryorderWorkflowUuid,
                'timesheet_fo_workflow_time_start' => new Expression('CURRENT_TIMESTAMP'),
                'user_uuid_create_start'           => $userUuidCreate,
            ]);
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function endTimesheetFoWorkflow(string $timesheetFoWorkflowUuid, string $userUuidCreate): bool
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'timesheet_fo_workflow_time_end' => new Expression('CURRENT_TIMESTAMP'),
                'user_uuid_create_end'           => $userUuidCreate,
            ]);
            $update->where(['timesheet_fo_workflow_uuid' => $timesheetFoWorkflowUuid]);
            return $this->updateWith($update) == 1;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $factoryorderUuid
     * @return array
     */
    public function getTimesheetsFoWorkflowForFactoryorder(string $factoryorderUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join(['fow' => 'factoryorder_workflow'], 'fow.factoryorder_workflow_uuid = timesheet_fo_workflow.factoryorder_workflow_uuid',
                FactoryorderWorkflowTable::getColumnsStatic(['factoryorder_workflow_uuid']), Select::JOIN_LEFT);
            $select->join(['fo' => 'factoryorder'], 'fo.factoryorder_uuid = fow.factoryorder_uuid',
                FactoryorderTable::getColumnsStatic(['factoryorder_uuid', 'user_uuid_create', 'user_uuid_update', 'equipment_uuid']), Select::JOIN_LEFT);
            $select->join(['p' => 'product'], 'p.product_uuid = fo.product_uuid',
                ['product_text_part', 'product_text_short', 'product_briefing'], Select::JOIN_LEFT);
            $select->join(['pn' => 'product_no'], 'pn.product_no_uuid = p.product_no_uuid',
                ['product_no_no'], Select::JOIN_LEFT);
            $select->where(['fo.factoryorder_uuid' => $factoryorderUuid]);
            $select->order('factoryorder_workflow_order_priority DESC');
            $q = $select->getSqlString($this->getAdapter()->platform);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
