<?php

namespace Lerp\Timesheet\Service\Equipment;

/**
 * Interface TimesheetSpreadsheetServiceInterface
 * Use this interface to implement your own TimesheetSpreadsheetService.
 * @package Lerp\Equipment\Service\Equipment
 */
interface TimesheetSpreadsheetServiceInterface
{
    /**
     * It creates a spreadsheet and send it to the browser as Microsoft Excel (.xlsx OpenXML) (application/vnd.openxmlformats-officedocument.spreadsheetml.sheet).
     * https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
     * @param string $equipmentUuid
     * @param string $timeStart
     * @param string $timeEnd
     */
    public function createTimesheetSpreadsheet(string $equipmentUuid, string $timeStart, string $timeEnd): void;
}
