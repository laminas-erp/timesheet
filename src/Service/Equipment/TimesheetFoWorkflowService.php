<?php

namespace Lerp\Timesheet\Service\Equipment;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Lerp\Timesheet\Table\Equipment\TimesheetFoWorkflowTable;
use Lerp\Timesheet\Table\Equipment\ViewTimesheetFoWorkflowTable;

class TimesheetFoWorkflowService extends AbstractService
{
    protected TimesheetFoWorkflowTable $timesheetFoWorkflowTable;
    protected ViewTimesheetFoWorkflowTable $viewTimesheetFoWorkflowTable;

    public function setTimesheetFoWorkflowTable(TimesheetFoWorkflowTable $timesheetFoWorkflowTable): void
    {
        $this->timesheetFoWorkflowTable = $timesheetFoWorkflowTable;
    }

    public function setViewTimesheetFoWorkflowTable(ViewTimesheetFoWorkflowTable $viewTimesheetFoWorkflowTable): void
    {
        $this->viewTimesheetFoWorkflowTable = $viewTimesheetFoWorkflowTable;
    }

    /**
     * Insert a new TimesheetFoWorkflow with the current timestamp as start time.
     * @param string $equipmentUuid
     * @param string $factoryorderWorkflowUuid
     * @param string $userUuidCreate
     * @return string
     */
    public function startTimesheetFoWorkflow(string $equipmentUuid, string $factoryorderWorkflowUuid, string $userUuidCreate): string
    {
        return $this->timesheetFoWorkflowTable->startTimesheetFoWorkflow($equipmentUuid, $factoryorderWorkflowUuid, $userUuidCreate);
    }

    /**
     * Update the end of a TimesheetFoWorkflow with the current timestamp.
     * @param string $timesheetFoWorkflowUuid
     * @param string $userUuidCreate
     * @return bool
     */
    public function endTimesheetFoWorkflow(string $timesheetFoWorkflowUuid, string $userUuidCreate): bool
    {
        return $this->timesheetFoWorkflowTable->endTimesheetFoWorkflow($timesheetFoWorkflowUuid, $userUuidCreate);
    }

    /**
     * Get all timesheetWorkflows for one factoryorder.
     * @param string $factoryorderUuid
     * @return array
     */
    public function getTimesheetsFoWorkflowForFactoryorder(string $factoryorderUuid): array
    {
        return $this->timesheetFoWorkflowTable->getTimesheetsFoWorkflowForFactoryorder($factoryorderUuid);
    }

    /**
     * Get all timesheetWorkflows for one factoryorder from view `view_timesheet_fo_workflow`.
     * @param string $factoryorderUuid
     * @return array
     */
    public function getViewTimesheetsFoWorkflowForFactoryorder(string $factoryorderUuid): array
    {
        return $this->viewTimesheetFoWorkflowTable->getViewTimesheetsFoWorkflowForFactoryorder($factoryorderUuid);
    }
}
