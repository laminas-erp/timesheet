<?php

namespace Lerp\Timesheet\Service\Equipment;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Lerp\Timesheet\Entity\ParamsTimesheetReport;
use Lerp\Equipment\Service\User\UserEquipService;
use Lerp\Timesheet\Table\Equipment\TimesheetFoWorkflowTable;
use Lerp\Timesheet\Table\Equipment\TimesheetTable;
use Lerp\Timesheet\Table\Equipment\ViewTimesheetTable;

class TimesheetService extends AbstractService
{
    public const START_END = ['start', 'end'];
    protected int $timesheetReportCount = 0;
    protected UserEquipService $equipmentUserService;
    protected TimesheetTable $timesheetTable;
    protected ViewTimesheetTable $viewTimesheetTable;

    public function getTimesheetReportCount(): int
    {
        return $this->timesheetReportCount;
    }

    public function setEquipmentUserService(UserEquipService $equipmentUserService): void
    {
        $this->equipmentUserService = $equipmentUserService;
    }

    public function setTimesheetTable(TimesheetTable $timesheetTable): void
    {
        $this->timesheetTable = $timesheetTable;
    }

    public function setViewTimesheetTable(ViewTimesheetTable $viewTimesheetTable): void
    {
        $this->viewTimesheetTable = $viewTimesheetTable;
    }

    /**
     * @param string $equipmentUuid
     * @param string $userUuidCreate
     * @param string $timeStart If it is empty, the current time is used.
     * @return string The new generated timesheet_uuid.
     */
    public function insertTimesheetStart(string $equipmentUuid, string $userUuidCreate, string $timeStart = ''): string
    {
        return $this->timesheetTable->insertTimesheetStart($equipmentUuid, $userUuidCreate, $timeStart);
    }

    /**
     * @param string $equipmentUuid
     * @param string $userUuidCreate
     * @param string $timeEnd If it is empty, the current time is used.
     * @return string The new generated timesheet_uuid or an empty string on failure.
     */
    public function insertTimesheetEnd(string $equipmentUuid, string $userUuidCreate, string $timeEnd = ''): string
    {
        return $this->timesheetTable->insertTimesheetEnd($equipmentUuid, $userUuidCreate, $timeEnd);
    }

    /**
     * @param string $startEnd Start or end? Use one of the values in TimesheetService::START_END
     * @return string The new generated timesheet_uuid or the count (must be 1) of UPDATE timesheet_end or an empty string on failure.
     */
    public function insertTimesheetUserNow(string $startEnd, string $userUuid): string
    {
        if (
            !in_array($startEnd, self::START_END)
            || empty($userEquip = $this->equipmentUserService->getUserEquipByUuid($userUuid))
        ) {
            return '';
        }
        return match ($startEnd) {
            self::START_END[0] => $this->timesheetTable->insertTimesheetStart($userEquip['equipment_uuid'], $userUuid),
            self::START_END[1] => $this->timesheetTable->insertTimesheetEnd($userEquip['equipment_uuid'], $userUuid),
            default => '',
        };
    }

    /**
     * @param string $userUuid
     * @return bool
     */
    public function isUserPresentNow(string $userUuid): bool
    {
        if (empty($userEquip = $this->equipmentUserService->getUserEquipByUuid($userUuid))) {
            return false;
        }
        return $this->timesheetTable->existTimesheetStartWithoutEnd($userEquip['equipment_uuid']);
    }

    /**
     * @param ParamsTimesheetReport $params
     * @param string $equipUuid
     * @param string $timeStart
     * @param string $timeEnd
     * @return array
     */
    public function createReport(ParamsTimesheetReport $params, string $equipUuid, string $timeStart, string $timeEnd): array
    {
        $params->setDoCount(true);
        $this->timesheetReportCount = $this->viewTimesheetTable->equipmentViewTimesheetPeriodSearch($params, $equipUuid, $timeStart, $timeEnd)[0];
        $params->setDoCount(false);
        return $this->viewTimesheetTable->equipmentViewTimesheetPeriodSearch($params, $equipUuid, $timeStart, $timeEnd);
    }
}
