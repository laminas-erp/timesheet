<?php

namespace Lerp\Timesheet\Controller\Pos\Equipment;

use Bitkorn\Trinket\View\Model\JsonModel;
use Laminas\Http\Response;
use Lerp\Timesheet\Controller\Pos\AbstractPosController;
use Lerp\Timesheet\Service\Equipment\TimesheetService;

class TimesheetPosController extends AbstractPosController
{
    /**
     * Get the data for one equipment by his equipment_key.
     *
     * @return JsonModel
     */
    public function equipmentDataAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->validApiKey()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (
            empty($equipmentKey = $this->params('equipment_key'))
            || empty($equipment = $this->userEquipService->getEquipmentByKey($equipmentKey))
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($equipment);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * Start or End now a timesheet entry for one equipment by his equipment_key.
     *
     * @return JsonModel
     */
    public function timesheetEquipmentNowAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->validApiKey()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (
            !$this->getRequest()->isPost()
            || empty($startEnd = $this->params()->fromPost('start_end'))
            || !in_array($startEnd, TimesheetService::START_END)
            || empty($equipmentKey = $this->params('equipment_key'))
            || empty($equipment = $this->userEquipService->getEquipmentByKey($equipmentKey))
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }

        if (!empty($value = $this->timesheetService->insertTimesheetUserNow($startEnd, $equipment['user_uuid']))) {
            $jsonModel->setVal($value);
            $jsonModel->setSuccess(1);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }
        return $jsonModel;
    }
}
