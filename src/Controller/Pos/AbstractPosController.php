<?php

namespace Lerp\Timesheet\Controller\Pos;

use Bitkorn\Trinket\Controller\AbstractHtmlController;
use Lerp\Equipment\Service\Equipment\EquipmentService;
use Lerp\Equipment\Service\User\UserEquipService;
use Lerp\Timesheet\Service\Equipment\TimesheetService;

abstract class AbstractPosController extends AbstractHtmlController
{
    protected string $apiKey;
    protected UserEquipService $userEquipService;
    protected EquipmentService $equipmentService;
    protected TimesheetService $timesheetService;

    public function setApiKey(string $apiKey): void
    {
        $this->apiKey = $apiKey;
    }

    public function setUserEquipService(UserEquipService $userEquipService): void
    {
        $this->userEquipService = $userEquipService;
    }

    public function setEquipmentService(EquipmentService $equipmentService): void
    {
        $this->equipmentService = $equipmentService;
    }

    public function setTimesheetService(TimesheetService $timesheetService): void
    {
        $this->timesheetService = $timesheetService;
    }

    protected function validApiKey(): bool
    {
        return (!empty($this->apiKey) && $this->apiKey === $this->params()->fromQuery('apikey'));
    }
}
