<?php

namespace Lerp\Timesheet\Controller\Ajax\Equipment;

use Bitkorn\Trinket\Validator\IsoDateTime;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Timesheet\Entity\ParamsTimesheetReport;
use Lerp\Equipment\Service\Equipment\EquipmentService;
use Lerp\Timesheet\Service\Equipment\TimesheetService;
use Lerp\Timesheet\Service\Equipment\TimesheetSpreadsheetServiceInterface;
use Lerp\Equipment\Service\User\UserEquipService;

class TimesheetAjaxController extends AbstractUserController
{
    protected TimesheetService $timesheetService;
    protected EquipmentService $equipmentService;
    protected UserEquipService $equipmentUserService;
    protected TimesheetSpreadsheetServiceInterface $timesheetSpreadsheetService;

    public function setTimesheetService(TimesheetService $timesheetService): void
    {
        $this->timesheetService = $timesheetService;
    }

    public function setEquipmentService(EquipmentService $equipmentService): void
    {
        $this->equipmentService = $equipmentService;
    }

    public function setEquipmentUserService(UserEquipService $equipmentUserService): void
    {
        $this->equipmentUserService = $equipmentUserService;
    }

    public function setTimesheetSpreadsheetService(TimesheetSpreadsheetServiceInterface $timesheetSpreadsheetService): void
    {
        $this->timesheetSpreadsheetService = $timesheetSpreadsheetService;
    }

    /**
     * Restricted access because it is timesheet for other user then me.
     * @return JsonModel
     */
    public function startAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (
            !$this->getRequest()->isPost()
            || empty($equipUuid = $this->params()->fromPost('equipment_uuid'))
            || !(new Uuid())->isValid($equipUuid)
            || (!empty($timeStart = $this->params()->fromPost('time_start', '')) && !(new IsoDateTime())->isValid($timeStart))
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($uuid = $this->timesheetService->insertTimesheetStart($equipUuid, $this->userService->getUserUuid(), $timeStart))) {
            $jsonModel->setUuid($uuid);
            $jsonModel->setSuccess(1);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }
        return $jsonModel;
    }

    /**
     * Restricted access because it is timesheet for other user then me.
     * @return JsonModel
     */
    public function endAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (
            !$this->getRequest()->isPost()
            || empty($equipUuid = $this->params()->fromPost('equipment_uuid'))
            || !(new Uuid())->isValid($equipUuid)
            || (!empty($timeEnd = $this->params()->fromPost('time_end', '')) && !(new IsoDateTime())->isValid($timeEnd))
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!empty($count = $this->timesheetService->insertTimesheetEnd($equipUuid, $this->userService->getUserUuid(), $timeEnd))) {
            $jsonModel->setCount($count);
            $jsonModel->setSuccess(1);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }
        return $jsonModel;
    }

    /**
     * Coming and going for the user of the session at the current time (now).
     *
     * @return JsonModel
     */
    public function timesheetMeNowAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (
            !$this->getRequest()->isPost()
            || empty($startEnd = $this->params()->fromPost('start_end'))
            || !in_array($startEnd, TimesheetService::START_END)
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($value = $this->timesheetService->insertTimesheetUserNow($startEnd, $this->userService->getUserUuid()))) {
            $jsonModel->setVal($value);
            $jsonModel->setSuccess(1);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }
        return $jsonModel;
    }

    /**
     * Check if a user is present now.
     *
     * @return JsonModel
     */
    public function userPresentNowAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (
            !$this->getRequest()->isPost()
            || empty($userUuid = $this->params('user_uuid'))
            || !(new Uuid())->isValid($userUuid)
            || !$this->userService->existUser($userUuid)
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setVal($this->timesheetService->isUserPresentNow($userUuid) ? 'true' : 'false');
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function reportAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $iso = new IsoDateTime();
        $params = new ParamsTimesheetReport();
        $params->setFromParamsArray($this->getRequest()->getPost()->toArray());
        if (
            !$this->getRequest()->isPost()
            || empty($equipUuid = $this->params()->fromPost('equipment_uuid'))
            || !(new Uuid())->isValid($equipUuid)
            || (!empty($dateStart = $this->params()->fromPost('date_start', '')) && !$iso->isValid($dateStart))
            || (!empty($dateEnd = $this->params()->fromPost('date_end', '')) && !$iso->isValid($dateEnd))
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->timesheetService->createReport($params, $equipUuid, $dateStart, $dateEnd));
        $jsonModel->setCount($this->timesheetService->getTimesheetReportCount());
        return $jsonModel;
    }

    /**
     * Create a spreadsheet for one equipment and send it to the client.
     *
     * @return JsonModel
     */
    public function reportSpreadsheetAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->userService->setSessionHashManually($this->params()->fromQuery('hash'));
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $iso = new IsoDateTime();
        if (
            !$this->getRequest()->isGet()
            || empty($equipUuid = $this->params()->fromQuery('equipment_uuid'))
            || !(new Uuid())->isValid($equipUuid)
            || (!empty($dateStart = $this->params()->fromQuery('date_start', '')) && !$iso->isValid($dateStart))
            || (!empty($dateEnd = $this->params()->fromQuery('date_end', '')) && !$iso->isValid($dateEnd))
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->timesheetSpreadsheetService->createTimesheetSpreadsheet($equipUuid, $dateStart, $dateEnd);
        return $jsonModel;
    }
}
