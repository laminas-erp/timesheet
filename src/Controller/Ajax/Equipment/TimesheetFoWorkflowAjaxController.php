<?php

namespace Lerp\Timesheet\Controller\Ajax\Equipment;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Timesheet\Service\Equipment\TimesheetFoWorkflowService;

class TimesheetFoWorkflowAjaxController extends AbstractUserController
{
    protected TimesheetFoWorkflowService $timesheetFoWorkflowService;

    public function setTimesheetFoWorkflowService(TimesheetFoWorkflowService $timesheetFoWorkflowService): void
    {
        $this->timesheetFoWorkflowService = $timesheetFoWorkflowService;
    }

    /**
     * Start a TimesheetFoWorkflow
     * @return JsonModel
     */
    public function foWorkflowStartAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->getRequest()->isPost()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        if (
            empty($equipUuid = $this->params()->fromPost('equipment_uuid'))
            || !($uuidValid = new Uuid())->isValid($equipUuid)
            || empty($foWorkflowUuid = $this->params()->fromPost('fo_workflow_uuid', ''))
            || !$uuidValid->isValid($foWorkflowUuid)
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($newUuid = $this->timesheetFoWorkflowService->startTimesheetFoWorkflow($equipUuid, $foWorkflowUuid, $this->userService->getUserUuid()))) {
            $jsonModel->setVal($newUuid);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * End a TimesheetFoWorkflow
     * @return JsonModel
     */
    public function foWorkflowEndAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->getRequest()->isPost()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        if (
            empty($timesheetFoWorkflowUuid = $this->params()->fromPost('timesheet_fo_workflow_uuid', ''))
            || !(new Uuid())->isValid($timesheetFoWorkflowUuid)
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->timesheetFoWorkflowService->endTimesheetFoWorkflow($timesheetFoWorkflowUuid, $this->userService->getUserUuid())) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    public function foWorkflowForFactoryorderAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (
            empty($foUuid = $this->params('fo_uuid'))
            || !(new Uuid())->isValid($foUuid)
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($timesheets = $this->timesheetFoWorkflowService->getViewTimesheetsFoWorkflowForFactoryorder($foUuid))) {
            $jsonModel->setArr($timesheets);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
