<?php

namespace Lerp\Timesheet;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Lerp\Timesheet\Controller\Ajax\Equipment\TimesheetAjaxController;
use Lerp\Timesheet\Controller\Ajax\Equipment\TimesheetFoWorkflowAjaxController;
use Lerp\Timesheet\Controller\Pos\Equipment\TimesheetPosController;
use Lerp\Timesheet\Factory\Controller\Ajax\Equipment\TimesheetAjaxControllerFactory;
use Lerp\Timesheet\Factory\Controller\Ajax\Equipment\TimesheetFoWorkflowAjaxControllerFactory;
use Lerp\Timesheet\Factory\Controller\Pos\Equipment\TimesheetPosControllerFactory;
use Lerp\Timesheet\Factory\Service\Equipment\TimesheetFoWorkflowServiceFactory;
use Lerp\Timesheet\Factory\Service\Equipment\TimesheetServiceFactory;
use Lerp\Timesheet\Factory\Table\Equipment\TimesheetFoWorkflowTableFactory;
use Lerp\Timesheet\Factory\Table\Equipment\TimesheetTableFactory;
use Lerp\Timesheet\Factory\Table\Equipment\ViewTimesheetFoWorkflowTableFactory;
use Lerp\Timesheet\Factory\Table\Equipment\ViewTimesheetTableFactory;
use Lerp\Timesheet\Service\Equipment\TimesheetFoWorkflowService;
use Lerp\Timesheet\Service\Equipment\TimesheetService;
use Lerp\Timesheet\Table\Equipment\TimesheetFoWorkflowTable;
use Lerp\Timesheet\Table\Equipment\TimesheetTable;
use Lerp\Timesheet\Table\Equipment\ViewTimesheetFoWorkflowTable;
use Lerp\Timesheet\Table\Equipment\ViewTimesheetTable;

return [
    'router'          => [
        'routes' => [
            /*
             * AJAX - equipment timesheet
             */
            'lerp_timesheet_ajax_equipment_start'              => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-timesheet-ajax-equipment-start',
                    'defaults' => [
                        'controller' => TimesheetAjaxController::class,
                        'action'     => 'start'
                    ],
                ],
            ],
            'lerp_timesheet_ajax_equipment_timesheetend'       => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-timesheet-ajax-equipment-end',
                    'defaults' => [
                        'controller' => TimesheetAjaxController::class,
                        'action'     => 'end'
                    ],
                ],
            ],
            'lerp_timesheet_ajax_equipment_menow'              => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-timesheet-ajax-equipment-me-now',
                    'defaults' => [
                        'controller' => TimesheetAjaxController::class,
                        'action'     => 'timesheetMeNow'
                    ],
                ],
            ],
            'lerp_timesheet_ajax_equipment_presentnow'         => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-timesheet-ajax-equipment-present-now/:user_uuid',
                    'constraints' => [
                        'user_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => TimesheetAjaxController::class,
                        'action'     => 'userPresentNow'
                    ],
                ],
            ],
            'lerp_timesheet_ajax_equipment_report'             => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-timesheet-ajax-equipment-report',
                    'defaults' => [
                        'controller' => TimesheetAjaxController::class,
                        'action'     => 'report'
                    ],
                ],
            ],
            'lerp_timesheet_ajax_equipment_reportspreadsheet'  => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-timesheet-ajax-equipment-report-spreadsheet',
                    'defaults' => [
                        'controller' => TimesheetAjaxController::class,
                        'action'     => 'reportSpreadsheet'
                    ],
                ],
            ],
            /*
             * AJAX - factoryorder workflow timesheet
             */
            'lerp_timesheet_ajax_fo_workflow_start'            => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-timesheet-ajax-fo-workflow-start',
                    'defaults' => [
                        'controller' => TimesheetFoWorkflowAjaxController::class,
                        'action'     => 'foWorkflowStart'
                    ],
                ],
            ],
            'lerp_timesheet_ajax_fo_workflow_end'              => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-timesheet-ajax-fo-workflow-end',
                    'defaults' => [
                        'controller' => TimesheetFoWorkflowAjaxController::class,
                        'action'     => 'foWorkflowEnd'
                    ],
                ],
            ],
            'lerp_timesheet_ajax_fo_workflow_for_factoryorder' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-timesheet-ajax-fo-workflow-for-factoryorder/:fo_uuid',
                    'constraints' => [
                        'fo_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => TimesheetFoWorkflowAjaxController::class,
                        'action'     => 'foWorkflowForFactoryorder'
                    ],
                ],
            ],
            /*
             * POS
             */
            'lerp_timesheet_pos_equipment_data' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-timesheet-pos-equipment-data/:equipment_key',
                    'constraints' => [
                        'equipment_key' => '[0-9A-Za-z-]+',
                    ],
                    'defaults'    => [
                        'controller' => TimesheetPosController::class,
                        'action'     => 'equipmentData'
                    ],
                ],
            ],
            'lerp_timesheet_pos_equipment_now' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-timesheet-pos-equipment-now/:equipment_key',
                    'constraints' => [
                        'equipment_key' => '[0-9A-Za-z-]+',
                    ],
                    'defaults'    => [
                        'controller' => TimesheetPosController::class,
                        'action'     => 'timesheetEquipmentNow'
                    ],
                ],
            ],
        ],
    ],
    'controllers'     => [
        'factories'  => [
            TimesheetAjaxController::class           => TimesheetAjaxControllerFactory::class,
            TimesheetFoWorkflowAjaxController::class => TimesheetFoWorkflowAjaxControllerFactory::class,
            TimesheetPosController::class            => TimesheetPosControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories'  => [
            // service
            TimesheetService::class             => TimesheetServiceFactory::class,
            TimesheetFoWorkflowService::class   => TimesheetFoWorkflowServiceFactory::class,
            // table
            TimesheetTable::class               => TimesheetTableFactory::class,
            ViewTimesheetTable::class           => ViewTimesheetTableFactory::class,
            ViewTimesheetFoWorkflowTable::class => ViewTimesheetFoWorkflowTableFactory::class,
            TimesheetFoWorkflowTable::class     => TimesheetFoWorkflowTableFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helpers'    => [
        'factories'  => [],
        'invokables' => [],
        'aliases'    => [],
    ],
    'view_manager'    => [
        'template_map'        => [],
        'template_path_stack' => [],
        'strategies'          => [
            'ViewJsonStrategy',
        ],
    ],
];
